import mpi.MPI;
import mpi.MPIException;
import mpi.Status;

import java.io.*;
import java.util.Arrays;
import java.util.Random;

public final class Main2 {

    public static void main(String[] args) throws MPIException, IOException, ClassNotFoundException {
        MPI.Init(args);

        final int me = MPI.COMM_WORLD.getRank();
        final int size = MPI.COMM_WORLD.getSize();

        System.out.println(String.format("Starting core %d, size %d", me, size));

        if (me == 2) {

            final Object[] arr = new Object[size];

            Arrays.fill(arr, randomFill());

            for (int i = 1; i < size; ++i) {
                bcastObjectArray(arr, i, 2);
                System.out.println(String.format("sending to core %d", i));
            }
        } else {
            System.out.println(String.format("Received %s", Arrays.toString(recvArrayOfObjects(size, 2, 0))));
        }

        MPI.Finalize();
    }


    public static double randomFill(){
        Random rand = new Random();
        return rand.nextInt();
    }

    public static Object bcastObject(Object o, int root)
            throws IOException, MPIException,
            ClassNotFoundException {
        byte[] tmp = null;
        int[] size = new int[1];
        int rank = MPI.COMM_WORLD.getRank();
        if (rank == root) {
            ByteArrayOutputStream bos =
                    new ByteArrayOutputStream();
            ObjectOutputStream oos =
                    new ObjectOutputStream(bos);
            oos.writeObject(o);
            tmp = bos.toByteArray();
            size[0] = tmp.length;
        }
        MPI.COMM_WORLD.bcast(size, 1, MPI.INT, root);
        if (rank != root) tmp = new byte[size[0]];
        MPI.COMM_WORLD.bcast(tmp, tmp.length,
                MPI.BYTE, root);
        if (rank != root) {
            ByteArrayInputStream bis =
                    new ByteArrayInputStream(tmp);
            ObjectInputStream ois =
                    new ObjectInputStream(bis);
            return ois.readObject();
        }
        return o;
    }

    public static void bcastObjectArray(Object[] o,
                                        int count, int root)
            throws IOException, MPIException, ClassNotFoundException {
        byte[] tmp = null;
        int[] size = new int[1];
        int rank = MPI.COMM_WORLD.getRank();
        if (rank == root) {
            ByteArrayOutputStream bos =
                    new ByteArrayOutputStream();
            ObjectOutputStream oos =
                    new ObjectOutputStream(bos);
            for (int i = 0; i < count; i++) oos.writeObject(o[i]);
            tmp = bos.toByteArray();
            size[0] = tmp.length;
        }
        MPI.COMM_WORLD.bcast(size, 1, MPI.INT, root);
        if (rank != root) tmp = new byte[size[0]];
        MPI.COMM_WORLD.bcast(tmp, tmp.length, MPI.BYTE, root);
        if (rank != root) {
            ByteArrayInputStream bis =
                    new ByteArrayInputStream(tmp);
            ObjectInputStream ois =
                    new ObjectInputStream(bis);
            for (int i = 0; i < count; i++) o[i] = ois.readObject();
        }
    }

    public static Object[] recvArrayOfObjects(int n, int proc,
                                              int tag)
            throws MPIException, IOException,
            ClassNotFoundException {
        Object[] o = new Object[n];

        for (int i = 0; i < n; i++)
            o[i] = recvObject(proc, tag + i);
        return o;
    }

    public static Object recvObject(int proc, int tag)
            throws MPIException, IOException,
            ClassNotFoundException {

        Status st = MPI.COMM_WORLD.probe(proc, tag);

        int size = st.getCount(MPI.BYTE);

        byte[] tmp = new byte[size];

        MPI.COMM_WORLD.recv(tmp, size, MPI.BYTE, proc, tag);
        Object res = null;
        ByteArrayInputStream bis =
                new ByteArrayInputStream(tmp);
        ObjectInputStream ois = new ObjectInputStream(bis);
        res = ois.readObject();

        return res;
    }
}